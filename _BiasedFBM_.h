#ifndef _LECORRELATED__H
#define _LECORRELATED__H

#include<vector>
#include<string>
#include<iterator>
#include<cmath>

#include"_Generic_Simulation_.h"
#include"_MPI_vector_.h"
#include"_SQLite_Database_.h"

#define OTHER_DATABASEMETHOD
#define NO_MEANS
//#define STATIONARY
#define NO_CONDITION
#include"_Parallelize_CondTimeSeries_.h"
#include"_Parallelize_CondTimeSeries_Multiply_.h"
/*#include "_GaussianPowerLawRNG_.h"*/
#include "_GaussianPowerLawRNGFFTW_.h"
#ifndef _SFMT_H_
#define _SFMT_H_
#include"dSFMT.h"
#endif

/*#include "boost/math/special_functions/log1p.hpp"*/
/*#include "boost/math/policies/policy.hpp"*/
/*typedef boost::math::policies::policy< boost::math::policies::digits10<1> > my_pol;*/

double fast_erfc(double x)
{
    // constants
    double a1 =  0.254829592;
    double a2 = -0.284496736;
    double a3 =  1.421413741;
    double a4 = -1.453152027;
    double a5 =  1.061405429;
    double p  =  0.3275911;

    // Save the sign of x
    int sign = 1;
    if (x < 0)
        sign = -1;
    x = fabs(x);

    // A&S formula 7.1.26
    double t = 1.0/(1.0 + p*x);
    double y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);

    return 1.0-sign*y;
}

inline float fast_log2 (float val)
{
   int * const    exp_ptr = reinterpret_cast <int *> (&val);
   int            x = *exp_ptr;
   const int      log_2 = ((x >> 23) & 255) - 128;
   x &= ~(255 << 23);
   x += 127 << 23;
   *exp_ptr = x;

   val = ((-1.0f/3) * val + 2) * val - 2.0f/3;   // (1)

   return (val + log_2);
}

inline float fast_log (const float &val)
{
   return (fast_log2 (val) * 0.69314718f);
}

/*****************************************

0-system size
1-bin width
2-gamma
3-t_init
4-t_end
5-tau
6-drift parameter
7-bin

8-NDisConf
9-count

*****************************************/
class _LECorrelated_ : public _Generic_Simulation_{
	private:
 const uint8_t NParameters = 11;
#ifndef _SAVEMOMENTUM_
 const uint8_t NResults = 2;
#else
 const uint8_t NResults = 10;
#endif // _SAVEMOMENTUM_
 uint64_t T = 0;

#if _SAVEMOMENTUM_ == 2

 _MPI_vector_<double> Position;

#ifdef _DECAYSIMULATIONS__
 _MPI_vector_<uint64_t> T_Array;
#endif //_DECAYSIMULATIONS__

#else //_SAVEMOMENTUM_==2
 double Position = 0.0;
#endif //_SAVEMOMENTUM_==2
 double gamma_ant = -500.;
 double LocalLambda = 0.;
 dsfmt_t dsfmt;
#ifndef __USEFLOATFORCORRELATIONARRAY__
 fftw_real *CorrelationArray;
#else
 fftwf_real *CorrelationArray;
#endif //__USEFLOATFORCORRELATIONARRAY__

 void AssignToPointers(void);
	public:
 std::string BaseName;
 _SQLite_Database_ Save;
 _MPI_vector_<double> Parameters;
 _MPI_vector_<double> Results;
 uint64_t Histogram;
 _GaussianPowerLaw_ CorrRNGGenerator;

 _LECorrelated_(void);
 _LECorrelated_(uint64_t);
 _LECorrelated_(unsigned long long , const char *, const char *);
// _LECorrelated_(unsigned long long seed, Type Nblocks, Type Degree);

 _LECorrelated_& Set_Parameters(void);
 bool Set_InitialConditions(void);
 _LECorrelated_& Simulate(void);
 _LECorrelated_& RandomSwap(void);
 _LECorrelated_& Save_Simulation(long long);
 long long Init_Database(void);
 void BeginTransaction(void);
 void EndTransaction(void);
/* inline double GetLambda(void);*/
};


_LECorrelated_::_LECorrelated_(void):
	Parameters(NParameters),
	Results(NResults),
	CorrRNGGenerator(time(NULL)){
 dsfmt_init_gen_rand(&dsfmt, time(NULL)+2);
 AssignToPointers();
}

_LECorrelated_::_LECorrelated_(uint64_t seed):
	Parameters(NParameters),
	Results(NResults),
	CorrRNGGenerator(seed+1ULL){
 dsfmt_init_gen_rand(&dsfmt, seed);
 AssignToPointers();
}


_LECorrelated_::_LECorrelated_(unsigned long long seed, const char *Database, const char *Table):
	BaseName(Table),
	Save(Database, Table),
	Parameters(NParameters),
	Results(NResults),
	CorrRNGGenerator(seed + 1ULL){
 dsfmt_init_gen_rand(&dsfmt, seed);
 Save.GetSet_MaxId();
 AssignToPointers();
}


//_LECorrelated_::_LECorrelated_(unsigned long long seed, Type Nblocks, Type Degree):
//	MaxTriang(Nblocks*(Degree+1)),
//	Network(Nblocks, Degree),
//	Parameters(NParameters),
//	Results(NResults),
//	EntropyHist(2*(Degree+1UL)*Nblocks+2+1){
// dsfmt_init_gen_rand(&dsfmt, seed);
// AssignToPointers();
//}


void _LECorrelated_::AssignToPointers(void){
 P2_Param = +Parameters;
 P2_Res = +Results;
 P2_Save = &Save;
}


_LECorrelated_& _LECorrelated_::Set_Parameters(void){
	/* Set_InitialConditions();*/
 return *this;
}


bool _LECorrelated_::Set_InitialConditions(void){
// Parameters[3] /= 2.0;
 T = 0;
#if _SAVEMOMENTUM_ == 2

 Position.Realloc((Parameters[0] - Parameters[9])/Parameters[6]);
 Position.Set_to_0();
#ifdef _DECAYSIMULATIONS__
 T_Array.Realloc((Parameters[0] - Parameters[9])/Parameters[6]);
 T_Array.Set_to_0();
#endif //_DECAYSIMULATIONS__

#else //_SAVEMOMENTUM_ == 2
 Position = 0.0;
#endif //_SAVEMOMENTUM_ == 2

 Histogram = Parameters[0]/Parameters[1]+1ULL;

#ifndef _DECAYSIMULATIONS__
 double poweroftwo = log2(Parameters[4])+1ULL;
#else // _DECAYSIMULATIONS__
 double poweroftwo = log2(Parameters[1])+1ULL;
#endif // _DECAYSIMULATIONS__

 if(fmod(poweroftwo, 1.0) != 0.0){
	std::cerr << "maximum time is not a power of two!!" << std::endl;
	exit(123);
 }
#ifndef _DECAYSIMULATIONS__
 if(CorrRNGGenerator.size() != 2ULL*Parameters[4]){
#else // _DECAYSIMULATIONS__
 if(CorrRNGGenerator.size() != 2ULL*Parameters[1]){
#endif //_DECAYSIMULATIONS__

	CorrRNGGenerator.SetForwardPlan(poweroftwo);
	CorrRNGGenerator.SetCorrelationArray(Parameters[2]);
	gamma_ant = Parameters[2];
	CorrRNGGenerator.SetBackwardPlan();
 }
 else if(fabs(Parameters[2] - gamma_ant) >= 0.00000001){
	CorrRNGGenerator.SetCorrelationArray(Parameters[2]);
	gamma_ant = Parameters[2];
 }
#ifndef _ZEROWIDTH_
 CorrelationArray = CorrRNGGenerator.GetCorrelatedRandomArray(0, Parameters[2]);
#endif //_ZEROWIDTH_
 return true;
}


_LECorrelated_& _LECorrelated_::Simulate(void){

#if !defined(_DECAYSIMULATIONS__)
 while( T < Parameters[3]){
	LocalLambda = CorrelationArray[T++];
#elif _SAVEMOMENTUM_ == 2 && defined(_DECAYSIMULATIONS__)
 for(uint32_t GammaIndex = 0; GammaIndex < Position.Get_size(); GammaIndex++){
 while( Position[GammaIndex] < Parameters[3] and T_Array[GammaIndex] < Parameters[1]){
#ifndef _ZEROWIDTH_
	LocalLambda = CorrelationArray[T_Array[GammaIndex]++];
#else
	LocalLambda = 0;T_Array[GammaIndex]++;
#endif //_ZEROWIDTH_

#else //!defined(_DECAYSIMULATIONS__)
// while( exp(Position) < Parameters[3] and T < Parameters[1]){
 while( Position < Parameters[3] and T < Parameters[1]){
	LocalLambda = CorrelationArray[T++];
#endif // _DECAYSIMULATIONS__

#if __DISTRIBUTION__ == 0 || !defined(__DISTRIBUTION__) //__BINARYDISTRIBUTION__
	LocalLambda = 1.-0.5*erfc(LocalLambda/sqrt(2.));
	LocalLambda = LocalLambda < Parameters[9] ? Parameters[6] : Parameters[8]*Parameters[6]; //LocalLambda = dsfmt_genrand_close_open(& CorrRNGGenerator.dsfmt);
	LocalLambda = LocalLambda-1.0;
#elif __DISTRIBUTION__ == 1 //__BOXDISTRIBUTION__
	LocalLambda = 1.-0.5*erfc(LocalLambda/sqrt(2.));
	LocalLambda = Parameters[9]*LocalLambda-1.0;
#elif __DISTRIBUTION__ == 2 //__GAUSSIANDISTRIBUTION__
	LocalLambda = Parameters[9]+Parameters[8]*LocalLambda-128.;
#endif //__DISTRIBUTION__


#if __DISTRIBUTION__ == 2 && _SAVEMOMENTUM_ == 2
#ifndef _DECAYSIMULATIONS__
	for(uint32_t GammaIndex = 0; GammaIndex < Position.Get_size(); GammaIndex++, LocalLambda += Parameters[6]){
#else //_DECAYSIMULATIONS__
		LocalLambda += GammaIndex*Parameters[6];
#endif //_DECAYSIMULATIONS__

//		Position[GammaIndex] += LocalLambda;
//		if( Position[GammaIndex] < 0.0)
//			Position[GammaIndex] *= -1.0;

		double temp_Position = Position[GammaIndex] + LocalLambda;
		if (temp_Position > 0.)
			Position[GammaIndex] = temp_Position;

//		double temp_Position = Position[GammaIndex] + LocalLambda;
//		if (temp_Position > 0.)
//			Position[GammaIndex] = temp_Position;

#ifdef _ZEROWIDTH_
		printf("%le\n",Position[GammaIndex]);
#endif
	}
#else //__DISTRIBUTION__ == 2 && _SAVEMOMENTUM_ == 2

//####### reflecting barrier
//	Position += LocalLambda;
//	if( Position < 0.0)
//		Position =Parameters[8];
//#############################

//####### exponential attenuation
//	if(LocalLambda < 0.)
//		Position += LocalLambda;
//	else
//		Position += LocalLambda;
//	Position += 0.5*LocalLambda*(1.+tanh((Position+LocalLambda)*Parameters[0]));
	double temp_Position = Position + LocalLambda;
	if (temp_Position > 0.)
		Position = temp_Position;
//	printf("%lf\n", Position);
//	if( Position <= 0.0)
//		Position =1./Parameters[0];
//###############################

#endif //__DISTRIBUTION__ == 2 && _SAVEMOMENTUM_ == 2
 }
#ifndef _SAVEMOMENTUM_
 uint64_t bin = Position + Parameters[1]/2. > Parameters[0] ? Histogram-1 : (Position + Parameters[1]/2.)/Parameters[1];
 Results[0] = bin;//printf("%lf\n", (double) bin);
 if(Position == 0.0){
	bin = 0;
 }
 else{
	double LogPos = log(Position) + log(Parameters[0]);
	double Width = 2.*log(Parameters[0])/(Histogram-1);
	bin = LogPos + Width/2. < 0.0 ? 0 : (LogPos+Width/2.)/Width;
	bin = bin > Histogram -1ULL ? Histogram -1ULL : bin;
 }
 Results[1] = bin;
#else //_SAVEMOMENTUM_

#ifndef _DECAYSIMULATIONS__

#if _SAVEMOMENTUM_ == 2
 uint64_t ttemp = 1ULL << 32;
 ttemp--;
 Results[8] = uint64_t(&Position[0]) & ttemp;
 Results[9] = (uint64_t(&Position[0]) >> 32) & ttemp;
#else //_SAVEMOMENTUM_ == 2
 Results[0] = 1.;
 Results[1] = std::numeric_limits<uint64_t>::max();
 Results[2] = Position;
 Results[3] = Position*Position;
 Results[4] = Results[3]*Position;
 Results[5] = Results[3]*Results[3];
 double rho = exp(-Position);
 Results[6] = rho;
 Results[7] = rho*rho;
 Results[8] = Results[7]*rho;
 Results[9] = Results[7]*Results[7];
#endif //_SAVEMOMENTUM_ == 2

#else // _DECAYSIMULATIONS__

#if _SAVEMOMENTUM_ == 2
 uint64_t ttemp = 1ULL << 32;
 ttemp--;
 Results[8] = uint64_t(&T_Array[0]) & ttemp;
 Results[9] = (uint64_t(&T_Array[0]) >> 32) & ttemp;
#else //_SAVEMOMENTUM_ == 2
 Results[0] = 1.;
 Results[1] = std::numeric_limits<uint64_t>::max();
 Results[2] = T;
 Results[3] = T*T;
 Results[4] = Results[3]*T;
 Results[5] = Results[3]*Results[3];
 double rho = exp(-Position);
 Results[6] = rho;
 Results[7] = rho*rho;
 Results[8] = Results[7]*rho;
 Results[9] = Results[7]*Results[7];
#endif //_SAVEMOMENTUM_ == 2

#endif //_DECAYSIMULATIONS__

#endif //_SAVEMOMENTUM_

 return *this;
}


_LECorrelated_& _LECorrelated_::Save_Simulation(long long Id){

#ifndef _SAVEMOMENTUM_
 if (Results[0] == 0.0 and Results[1] == 0.0){
	return *this;
 }
 Save.Exec("Update "+Save.Get_TableName()+" SET count = count+"+std::to_string(Results[0])+", count_ln = count_ln + "+std::to_string(Results[1])+" WHERE Id ="+std::to_string(Id)+";");
#else
 _MPI_vector_<double> Temp(NResults);//printf("%lu\n", Id);
 Save.SearchforId( Id, Temp.Get_Pointer());
 if(Temp[1] != 0.)
	Results[1] = Results[1] < Temp[1] ? Results[1] : Temp[1];

 std::string Command = "Update "+Save.Get_TableName()+" SET count=count+"+std::to_string(Results[0])+", ";
 Command += "MinTime = "+std::to_string(Results[1])+", ";
 Command += "x1 = x1 + "+std::to_string(Results[2])+", ";
 Command += "x2 = x2 + "+std::to_string(Results[3])+", ";
 Command += "x3 = x3 + "+std::to_string(Results[4])+", ";
 Command += "x4 = x4 + "+std::to_string(Results[5])+", ";
 Command += "rho1 = rho1 + "+std::to_string(Results[6])+", ";
 Command += "rho2 = rho2 + "+std::to_string(Results[7])+", ";
 Command += "rho3 = rho3 + "+std::to_string(Results[8])+", ";
 Command += "rho4 = rho4 + "+std::to_string(Results[9])+" ";
 Command += "WHERE Id ="+std::to_string(Id)+";";
 Save.Exec(Command);
#endif// _SAVEMOMENTUM_
 return *this;
}


long long _LECorrelated_::Init_Database(void){
 return Save.AssignId(Parameters.Get_Pointer());
}

void _LECorrelated_::BeginTransaction(void){
 Save.Exec("BEGIN TRANSACTION");
}

void _LECorrelated_::EndTransaction(void){
 Save.Exec("END TRANSACTION");
}
/***********************************************
l
   RELATED TO _Parallelize_CondTimeSeries_.h

***********************************************/

void _Parallelize_CondTimeSeries_::Save_TimeSeries(void){
 time_t t1, t2;
 time(&t1);
 Par_Simul->BeginTransaction();
 unsigned long long MaxBins = TMP_Serie.Get_Received_size();
// (*Par_Simul->P2_Param) = TMP_Serie.Parameters;
 for(unsigned long long bin = 0; bin < MaxBins; bin++){
//	(*Par_Simul->P2_Param)[7] = bin;
	TMP_Serie.Get_line(*Par_Simul->P2_Res, bin);
	Par_Simul->Save_Simulation(TMP_Serie.Id[bin]);
 }
 Par_Simul->EndTransaction();
 time(&t2);
 printf("Time to save %lf\n", difftime(t2, t1));
}

unsigned long long _Parallelize_CondTimeSeries_::Init_Database(_Time_Series_ &TMP){
 time_t t1, t2;
 time(&t1);
 uint64_t Niter = Get_Niter(TMP.Parameters[4], TMP.Parameters[3], TMP.Parameters[5]);
// Niter++;
#ifndef _SAVEMOMENTUM_
 unsigned long long MaxBins = TMP.Parameters[0]/TMP.Parameters[1] + 1.0;
#elif _SAVEMOMENTUM_ == 2
 unsigned long long MaxBins = (TMP.Parameters[0] - TMP.Parameters[9])/TMP.Parameters[6];
 double temp0 = TMP.Parameters[0];
 double temp6 = TMP.Parameters[6];
 double temp9 = TMP.Parameters[9];
 TMP.Parameters[0] = 0.;
 TMP.Parameters[6] = 0.;
 TMP.Parameters[9] = 0.;
#else // _SAVEMOMENTUM_
 unsigned long long MaxBins = 1;
#endif //_SAVEMOMENTUM_
 if( MaxBins*Niter > TMP.Get_Res_ArraySize()) TMP.Realloc_TimeSeries(MaxBins*Niter);
 double temp = TMP.Parameters[3];
 double temp4 = TMP.Parameters[4];
 double temp5 = TMP.Parameters[5];
 TMP.Parameters[4] = 0.;
#ifdef _ZEROPARAM5_
 TMP.Parameters[5] = 0.;
#endif
 Par_Simul->BeginTransaction();
 uint64_t MaxId = Par_Simul->P2_Save->GetSet_MaxId();
 for(uint64_t iter = 0; iter < Niter; AssignTime(*TMP.Parameters, iter, temp5)){
	(*Par_Simul->P2_Param) = TMP.Parameters;
	unsigned long long bin = 0;
	(*Par_Simul->P2_Param)[7] = bin;
	#if _SAVEMOMENTUM_ != 2
	uint32_t NumberofMatches = Par_Simul->P2_Save->Searchfor(Par_Simul->P2_Param->Get_Pointer());
	if(NumberofMatches == 0){
		for(; bin < MaxBins; bin++){
			(*Par_Simul->P2_Param)[7] = bin;
			TMP.Id[bin+iter*MaxBins] = ++MaxId;
			Par_Simul->P2_Save->InsertId(Par_Simul->P2_Param->Get_Pointer(), MaxId);
		}
	}
	else if(NumberofMatches == 1){
		TMP.Id[iter*MaxBins] = Par_Simul->Init_Database();
		uint64_t TempId = TMP.Id[iter*MaxBins];
		bin++;
		for(; bin < MaxBins; bin++){
			TMP.Id[bin+iter*MaxBins] = TempId+bin;
		}
	}
	else{
		std::cout << "Wrong number of matches, please check database insertion" << std::endl;
		exit(12);
	}
	#elif _SAVEMOMENTUM_ == 2
	for(; bin < MaxBins; bin++){
		(*Par_Simul->P2_Param)[9] = temp9 + bin*temp6;
		uint32_t NumberofMatches = Par_Simul->P2_Save->Searchfor(Par_Simul->P2_Param->Get_Pointer());
		if(NumberofMatches == 0){
			TMP.Id[bin+iter*MaxBins] = ++MaxId;
			Par_Simul->P2_Save->InsertId(Par_Simul->P2_Param->Get_Pointer(), MaxId);			
		}
		else if(NumberofMatches == 1){
			TMP.Id[iter*MaxBins+bin] = Par_Simul->Init_Database();
/*			uint64_t TempId = TMP.Id[iter*MaxBins];*/
/*			TMP.Id[bin+iter*MaxBins] = TempId+bin;*/
		}
		else{
			std::cout << "Wrong number of matches, please check database insertion" << std::endl;
			exit(12);
		}
	}
	#endif //_SAVEMOMENTUM_ == 2
	iter++;
 }
 Par_Simul->EndTransaction();
 Par_Simul->P2_Save->GetSet_MaxId();
 TMP.Parameters[3] = temp;
 TMP.Parameters[4] = temp4;
 TMP.Parameters[5] = temp5;
#if _SAVEMOMENTUM_ == 2
 TMP.Parameters[0] = temp0;
 TMP.Parameters[6] = temp6;
 TMP.Parameters[9] = temp9;
#endif //_SAVEMOMENTUM_ == 2
 time(&t2);
 printf("Database Init time %lf\n", difftime(t2, t1));
 return MaxBins*Niter;
}


void _Parallelize_CondTimeSeries_::no_means_func(_MPI_vector_<double> &Results, unsigned long long x){
#ifndef _SAVEMOMENTUM_
 uint64_t histsize = Get_Niter(TMP_Serie.Parameters[4], TMP_Serie.Parameters[3], TMP_Serie.Parameters[5]);
 histsize = TMP_Serie.Get_Received_size()/histsize;
 TMP_Serie.Res_Array[Results.Get_size()*(Results[0]+histsize*x)] += 1.0;
 TMP_Serie.Res_Array[Results.Get_size()*(Results[1]+histsize*x)+1] += 1.0;

#elif _SAVEMOMENTUM_==2
 uint64_t histsize = (TMP_Serie.Parameters[0] - TMP_Serie.Parameters[9])/TMP_Serie.Parameters[6];
#if defined(_DECAYSIMULATIONS__)
 uint64_t *Res_Array = (uint64_t*) ( uint64_t(Results[8]) + (uint64_t(Results[9]) << 32) );
#else //defined(_DECAYSIMULATIONS__)
 double *Res_Array = (double*) ( uint64_t(Results[8]) + (uint64_t(Results[9]) << 32) );
#endif //defined(_DECAYSIMULATIONS__)
 for(uint32_t index = 0; index < histsize; index++){
	uint64_t offset = Results.Get_size()*histsize*x + Results.Get_size()*index;
/*	printf("offset %lu %lu\n", offset, TMP_Serie.Get_Received_size());*/
	TMP_Serie.Res_Array[offset+0] += 1.0;
	TMP_Serie.Res_Array[offset+1]  = 0.;
	TMP_Serie.Res_Array[offset+2] += Res_Array[index];
	TMP_Serie.Res_Array[offset+3] += Res_Array[index]*Res_Array[index];
	TMP_Serie.Res_Array[offset+4] += pow(Res_Array[index], 3);
	TMP_Serie.Res_Array[offset+5] += pow(Res_Array[index], 4);
#if defined(_DECAYSIMULATIONS__)
	double rho = exp(-double(Res_Array[index]));
	TMP_Serie.Res_Array[offset+6] += rho;
	TMP_Serie.Res_Array[offset+7] += rho*rho;
	TMP_Serie.Res_Array[offset+8] += pow(rho, 3);
	TMP_Serie.Res_Array[offset+9] += pow(rho, 4);
#else //defined(_DECAYSIMULATIONS__)
	double rho = exp(-Res_Array[index]);
	TMP_Serie.Res_Array[offset+6] += rho;
	TMP_Serie.Res_Array[offset+7] += rho*rho;
	TMP_Serie.Res_Array[offset+8] += pow(rho, 3);
	TMP_Serie.Res_Array[offset+9] += pow(rho, 4);
#endif //defined(_DECAYSIMULATIONS__)
 }

#else //_SAVEMOMENTUM_

 TMP_Serie.Res_Array[Results.Get_size()*x] += Results[0];
 if(TMP_Serie.Res_Array[Results.Get_size()*x+1] == 0.){
	TMP_Serie.Res_Array[Results.Get_size()*x+1] = Results[1];
 }
 else{
	TMP_Serie.Res_Array[Results.Get_size()*x] = TMP_Serie.Res_Array[Results.Get_size()*x] > Results[1] ? Results[1] : TMP_Serie.Res_Array[Results.Get_size()*x];
 }
 TMP_Serie.Res_Array[Results.Get_size()*x+2] += Results[2];
 TMP_Serie.Res_Array[Results.Get_size()*x+3] += Results[3];
 TMP_Serie.Res_Array[Results.Get_size()*x+4] += Results[4];
 TMP_Serie.Res_Array[Results.Get_size()*x+5] += Results[5];
 TMP_Serie.Res_Array[Results.Get_size()*x+6] += Results[6];
 TMP_Serie.Res_Array[Results.Get_size()*x+7] += Results[7];
 TMP_Serie.Res_Array[Results.Get_size()*x+8] += Results[8];
 TMP_Serie.Res_Array[Results.Get_size()*x+9] += Results[9];
#endif // _SAVEMOMENTUM_
}

void _Parallelize_CondTimeSeries_::no_means_BEFORESEND_func(_MPI_vector_<double> &Results){
 unsigned long long MaxBins = TMP_Serie.Get_Received_size();
// TMP_Serie.Parameters[4] = 0.0;
// TMP_Serie.Parameters[5] = 0.0;

// for(unsigned long long bin = 0; bin < MaxBins; bin++){
//	TMP_Serie.Res_Array[Results[0]] += 1.0;
//	TMP_Serie.Res_Array[bin*Results[Results.Get_size()-1]] = 1.0;
// }
 TMP_Serie.Send_Partial_TimeSeries(0, 0, MaxBins);
}
#endif


