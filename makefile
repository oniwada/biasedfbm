FLAGS=-O3 --fast-math -Wall -ansi -std=c++11 -fno-strict-aliasing -I../GeneralMonteCarlo -I../CorrelatedRNG -I../dSFMT -I../GeneralMonteCarlo -msse2 -DHAVE_SSE2 -DDSFMT_MEXP=521 -Wformat=0
CC=mpiCC
DEPS=../GeneralMonteCarlo/_Generic_Simulation_.h ../GeneralMonteCarlo/_MPI_vector_.h ../GeneralMonteCarlo/_SQLite_Database_.h ../GeneralMonteCarlo/_Parallelize_CondTimeSeries_.h ../dSFMT/dSFMT.c ../dSFMT/dSFMT.h ../CorrelatedRNG/_GaussianPowerLawRNG_.h _BiasedFBM_.h
INC_LIB=-lm -lsqlite3

run: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o run -DOTHER_NITER ../dSFMT/dSFMT.c run.cpp $(INC_LIB) 

run-Amalg: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o run-Amalg -DOTHER_NITER -D__SQLITEAMALGAMATION__ -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c run.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lm -ldl

run-float: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o run-float -DOTHER_NITER -D__SQLITEAMALGAMATION__ -D__USEFLOATFORCORRELATIONARRAY__ -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c run.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lm -ldl

run-fftw: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o run-float -DOTHER_NITER -D__SQLITEAMALGAMATION__ -I../fft_MIT/double/include/ -I../fft_MIT/double/bin/ -L../fft_MIT/double/lib -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c run.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lfftw3 -lm -ldl

run-Gaussian: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o run-Gaussian -D__DISTRIBUTION__=2 -DOTHER_NITER -D__SQLITEAMALGAMATION__ -D__USEFLOATFORCORRELATIONARRAY__ -I../fft_MIT/float/include/ -I../fft_MIT/float/bin/ -L../fft_MIT/float/lib -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c run.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lfftw3f -lm -ldl

Momentus: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o Momentus -D_SAVEMOMENTUM_ -DOTHER_NITER -D__SQLITEAMALGAMATION__ -D__USEFLOATFORCORRELATIONARRAY__ -I../fft_MIT/float/include/ -I../fft_MIT/float/bin/ -L../fft_MIT/float/lib -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c run.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lfftw3f -lm -ldl

Momentus-Gaussian: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o Momentus-Gaussian -D__DISTRIBUTION__=2 -D_SAVEMOMENTUM_=2 -DOTHER_NITER -D__SQLITEAMALGAMATION__ -D__USEFLOATFORCORRELATIONARRAY__ -I../fft_MIT/float/include/ -I../fft_MIT/float/bin/ -L../fft_MIT/float/lib -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c run.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lfftw3f -lm -ldl

Momentus-GaussianDouble: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o Momentus-GaussianDouble -D_ZEROPARAM5_ -D__DISTRIBUTION__=2 -D_SAVEMOMENTUM_=2 -DOTHER_NITER -D__SQLITEAMALGAMATION__ -I../fft_MIT/double/include/ -I../fft_MIT/double/bin/ -L../fft_MIT/double/lib -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c run.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lfftw3 -lm -ldl

Time: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o Time -D_SAVEMOMENTUM_ -D_DECAYSIMULATIONS__ -DOTHER_NITER -D__SQLITEAMALGAMATION__ -D__USEFLOATFORCORRELATIONARRAY__ -I../fft_MIT/float/include/ -I../fft_MIT/float/bin/ -L../fft_MIT/float/lib -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c run.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lfftw3f -lm -ldl

TimeBox: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o Time -D_SAVEMOMENTUM_ -D_DECAYSIMULATIONS__ -DOTHER_NITER -D__SQLITEAMALGAMATION__ -D__DISTRIBUTION__=1 -D__USEFLOATFORCORRELATIONARRAY__ -I../fft_MIT/float/include/ -I../fft_MIT/float/bin/ -L../fft_MIT/float/lib -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c run.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lfftw3f -lm -ldl

Time-Gaussian: $(DEPS) runLinear.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o Time-Gaussian -D_SAVEMOMENTUM_=2 -D_DECAYSIMULATIONS__ -DOTHER_NITER -D__SQLITEAMALGAMATION__ -D__DISTRIBUTION__=2 -D__USEFLOATFORCORRELATIONARRAY__ -I../fft_MIT/float/include/ -I../fft_MIT/float/bin/ -L../fft_MIT/float/lib -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c runLinear.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lfftw3f -lm -ldl

Time-GaussianZeroWidth: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o Time-GaussianZeroWidth -D_ZEROWIDTH_ -D_SAVEMOMENTUM_=2 -D_DECAYSIMULATIONS__ -DOTHER_NITER -D__SQLITEAMALGAMATION__ -D__DISTRIBUTION__=2 -D__USEFLOATFORCORRELATIONARRAY__ -I../fft_MIT/float/include/ -I../fft_MIT/float/bin/ -L../fft_MIT/float/lib -I../SQLiteAmalg/sqlite ../dSFMT/dSFMT.c run.cpp ../SQLiteAmalg/sqlite/sqlite3.o -lfftw3f -lm -ldl

teste: $(DEPS) run.cpp
	@clear
	@clear
	$(CC) $(FLAGS) -o teste -DOTHER_NITER ../dSFMT/dSFMT.c run.cpp $(INC_LIB) 
