#include"_BiasedFBM_.h"
#include<random>
#define DATABASE ":memory:"
int main(int Nargs, char*Inputs[]){

 int rank, size;
 MPI_Init(&Nargs, &Inputs);
 MPI_Comm_rank(MPI_COMM_WORLD, &rank);
 MPI_Comm_size(MPI_COMM_WORLD, &size);
 uint64_t NMeans = 200;
 if(rank == 0){
	std::random_device rnd;
	_MPI_vector_<long long> seed(1);
	for(int process = 1; process < size; process++){
		seed[0] = rnd();
		seed.Send(process, 0);
	}
	_LECorrelated_ Simul(time(NULL),Inputs[1],Inputs[2]);
	Simul.Save.Exec("PRAGMA page_size=262144;");
	Simul.Save.Exec("PRAGMA cache_size=100000;");
	Simul.Save.Exec("PRAGMA temp_store=2;");
	Simul.Save.Exec("PRAGMA synchronous=0;");
	Simul.Save.Exec("PRAGMA mmap_size=2097152;");
	Simul.Save.Exec("PRAGMA threads=4;");
//	Simul.Save.Exec("PRAGMA LOCKING_MODE = EXCLUSIVE;");
	Simul.Save.Exec("VACUUM;");
	Simul.Save.Exec("PRAGMA threads;PRAGMA cache_size;PRAGMA synchronous;", _SQLite_Func_::print);
//	_Parallelize_CondTimeSeries_Multiply_ ParSimul(+Simul, Inputs[3], rank, size);
	_Parallelize_CondTimeSeries_ ParSimul(+Simul, Inputs[3], rank, size);
	ParSimul.Set_NTS_perSlave(NMeans);
	ParSimul.run();
 }
 else{
	_MPI_vector_<long long> seed(1);
	seed.Recv(0, 0);
	_LECorrelated_ Simul(seed[0]);
//	_Parallelize_CondTimeSeries_Multiply_ ParSimul(+Simul, rank, size);
	_Parallelize_CondTimeSeries_ ParSimul(+Simul, rank, size);
	ParSimul.Set_NTS_perSlave(NMeans);
	ParSimul.run();
 }

 MPI_Finalize();
 return 0;
}
